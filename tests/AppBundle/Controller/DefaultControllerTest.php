<?php

namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DefaultControllerTest extends WebTestCase
{
    public function testDashboardIsShown()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/');

        //  First, i check there is 4 inputs
        $this->assertCount(4, $crawler->filter('form input[type=text]'));
    }

    public function testAllInputsAreRequired()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/');

        //  Now, i check that all fields are required
        $form = $crawler->filter('form button')->selectButton('Generate robot roster!')->form();
        $crawler = $client->submit($form);

        $this->assertContains('This value should not be blank.', $crawler->filter('.form-group')->eq(0)->text());
        $this->assertContains('This value should not be blank.', $crawler->filter('.form-group')->eq(1)->text());
        $this->assertContains('This value should not be blank.', $crawler->filter('.form-group')->eq(2)->text());
        $this->assertContains('This value should not be blank.', $crawler->filter('.form-group')->eq(3)->text());
    }

    public function testAllMinimumValuesAreChecked() {
        $client = static::createClient();

        $crawler = $client->request('GET', '/');
        $form = $crawler->filter('form button')->selectButton('Generate robot roster!')->form();
        $crawler = $client->submit($form, array(
                'roster_form[roster_max_points]' => -1,
                'roster_form[starters_quantity]' => 0,
                'roster_form[substitutes_quantity]' => 0,
                'roster_form[robot_max_score]' => -1
        ));

        $this->assertContains('This field must have a value higher or equal than 0', $crawler->filter('.form-group')->eq(0)->text());
        $this->assertContains('This field must have a value higher or equal than 1', $crawler->filter('.form-group')->eq(1)->text());
        $this->assertContains('This field must have a value higher or equal than 1', $crawler->filter('.form-group')->eq(2)->text());
        $this->assertContains('This field must have a value higher or equal than 0', $crawler->filter('.form-group')->eq(3)->text());
    }

    public function testRosterScoreMustBeHigherThanRobotScore() {
        $client = static::createClient();

        $crawler = $client->request('GET', '/');
        $form = $crawler->filter('form button')->selectButton('Generate robot roster!')->form();
        $crawler = $client->submit($form, array(
            'roster_form[roster_max_points]' => 50,
            'roster_form[starters_quantity]' => 1,
            'roster_form[substitutes_quantity]' => 1,
            'roster_form[robot_max_score]' => 51
        ));

        $this->assertContains(
            'Robots can\'t have a max score higher than the roster',
            $crawler->filter('form .alert-danger')->eq(0)->text()
        );
    }

    public function testRobotMaxScoreMustBeHigherThanRobotsQuantity() {
        $client = static::createClient();

        $crawler = $client->request('GET', '/');
        $form = $crawler->filter('form button')->selectButton('Generate robot roster!')->form();
        $crawler = $client->submit($form, array(
            'roster_form[roster_max_points]' => 100,
            'roster_form[starters_quantity]' => 10,
            'roster_form[substitutes_quantity]' => 1,
            'roster_form[robot_max_score]' => 10
        ));

        $this->assertContains(
            'The robots max score must be at least equal to the total number of robots in the roster',
            $crawler->filter('form .alert-danger')->eq(0)->text()
        );
    }

    public function testRosterMaxScoreMustAllowUniqueRobotsScores() {
        $client = static::createClient();

        $crawler = $client->request('GET', '/');
        $form = $crawler->filter('form button')->selectButton('Generate robot roster!')->form();
        $crawler = $client->submit($form, array(
            'roster_form[roster_max_points]' => 5,
            'roster_form[starters_quantity]' => 3,
            'roster_form[substitutes_quantity]' => 1,
            'roster_form[robot_max_score]' => 4
        ));

        $this->assertContains(
            'The roster max score is too low for so many robots',
            $crawler->filter('form .alert-danger')->eq(0)->text()
        );
    }

    public function testRosterWorksFine() {
        $client = static::createClient();

        $crawler = $client->request('GET', '/');
        $form = $crawler->filter('form button')->selectButton('Generate robot roster!')->form();
        $crawler = $client->submit($form, array(
            'roster_form[roster_max_points]' => 175,
            'roster_form[starters_quantity]' => 10,
            'roster_form[substitutes_quantity]' => 5,
            'roster_form[robot_max_score]' => 100
        ));

        //  assert that the 15 robots were generated
        $robots = $crawler->filter('.js-robot-score-value');
        $this->assertCount(15, $robots);


        $this->assertLessThanOrEqual(175, array_sum($robots->extract(array('_text'))));
    }
}
