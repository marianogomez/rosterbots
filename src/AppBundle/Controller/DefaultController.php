<?php

namespace AppBundle\Controller;

use AppBundle\Form\RosterForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{

    /**
     * @Route("/", name="roster_dashboard")
     */
    public final function showDashboard(Request $request) {
        $form = $this->createForm(RosterForm::class);
        $roster = null;

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $roster_service = $this->get('app.roster_generator_service');
            try {
                $roster = $roster_service->createRoster($form->getData());
            } catch (Exception $e) {
                $form->addError(new FormError($e->getMessage()));
            }
        }

        return $this->render('default/dashboard.html.twig', array(
            'form' => $form->createView(),
            'roster' => $roster,
            'robot_max_score' => intval($form->get('robot_max_score')->getData()),
        ));
    }

}