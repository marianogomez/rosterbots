<?php

namespace AppBundle\Entity;

class Robot {

    const ROBOT_MAX_SCORE = 100;
    const NAME_VALID_CHARACTERS = 'abcdefghijklmnopqrstuvwxyz';
    const NAME_VALID_CHARACTERS_COUNT = 26;
    const NAME_MIN_CHARACTERS = 3;
    const NAME_MAX_CHARACTERS = 6;

    private $name;

    private $speed;

    private $strength;

    private $agility;

    private $score;

    private $role;

    function __construct($score)
    {
        $this->setName($this->generateRandomName() . $score);  //  Since $robot_score is unique, this name will also be unique
        $this->setScore($score);
        $this->setAgility(rand(0, $this->getScore()));
        $this->setSpeed(rand(0, $this->getScore()-$this->getAgility()));
        $this->setStrength($this->getScore() - $this->getAgility() - $this->getSpeed());
    }

    public function generateRandomName() {
        $characters = rand(self::NAME_MIN_CHARACTERS, self::NAME_MAX_CHARACTERS);
        $random_name = '';
        for ($i = 0; $i<$characters; $i++) {
            $random_name .= substr(self::NAME_VALID_CHARACTERS, rand(0, self::NAME_VALID_CHARACTERS_COUNT-1), 1);
        }
        return strtoupper($random_name);
    }
    /**
     * @return int
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param int $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getSpeed()
    {
        return $this->speed;
    }

    /**
     * @param int $speed
     */
    public function setSpeed($speed)
    {
        $this->speed = $speed;
    }

    /**
     * @return int
     */
    public function getStrength()
    {
        return $this->strength;
    }

    /**
     * @param int $strength
     */
    public function setStrength($strength)
    {
        $this->strength = $strength;
    }

    /**
     * @return int
     */
    public function getAgility()
    {
        return $this->agility;
    }

    /**
     * @param int $agility
     */
    public function setAgility($agility)
    {
        $this->agility = $agility;
    }

    /**
     * @return int
     */
    public function getScore()
    {
        return $this->score;
    }

    /**
     * @param int $score
     */
    public function setScore($score)
    {
        $this->score = $score;
    }

    /**
     * @return string
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * @param string $role
     */
    public function setRole($role)
    {
        $this->role = $role;
    }


}