<?php

namespace AppBundle\Service;

use AppBundle\Entity\Robot;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\DependencyInjection\ContainerInterface;

class RosterGeneratorService
{

    const ROSTER_CREDIT = 175;
    const ROBOTS_IN_ROSTER = 15;
    const ROLE_STARTER = "starter";
    const ROLE_SUBSTITUTE = "substitute";

    private $container;
    private $roster;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public final function createRoster($arguments) {

        $this->log('', true);
        $roster = array();

        //  If the maximum robot score is higher than the roster max score, i throw an exception
        if ($arguments['robot_max_score'] > $arguments['roster_max_points']) {
            throw new Exception("Robots can't have a max score higher than the roster");
        }


        //  I check how many starters and how many susbtitutes should be on the roster
        $starter_roles_quantity = intval($arguments['starters_quantity']);
        $substitute_roles_quantity = intval($arguments['substitutes_quantity']);
        $total_robots = $starter_roles_quantity + $substitute_roles_quantity;

        if ($arguments['robot_max_score'] < $total_robots) {
            throw new Exception("The robots max score must be at least equal to the total number of robots in the roster");
        }


        //  Now, let's create an array with as many starter and substitute roles as we need, so we can later assign them
        //  to the robots
        $roster_roles = $this->generateRosterRolesArray($starter_roles_quantity, $substitute_roles_quantity);

        //  First, i generate an array with the minimum possible roster score. This array will be used as a "cotrol"
        //  element so we don't fall in the case in which the roster maximum score is reached and we sill need to
        //  create more robots
        $unused_minimum_scores = array();
        for ($i = 0; $i<$total_robots; $i++) {
            $unused_minimum_scores[] = $i;
        }

        //  Now, i check that the sum of the minimum N values (N being the total of robots) is higher than the
        //  maximum roster allowed score, i throw an exception
        if (array_sum($unused_minimum_scores) > $arguments['roster_max_points']) {
            throw new Exception("The roster max score is too low for so many robots");
        }

        $used_scores = array();

        for ($i=0; $i < $total_robots; $i++) {
            $max = $arguments['roster_max_points']
                - array_sum($used_scores)
                - array_sum($unused_minimum_scores)
                + $unused_minimum_scores[count($unused_minimum_scores)-1];

            $min = $unused_minimum_scores[0];

            //  We have to get a score for our new robot that is between the valid period, and that is also available
            //  (that means there isn't another robot with the same score)
            do {
                $robot_score = rand($min, $max);
            } while (in_array($robot_score, $used_scores));

            $robot_score_position = array_search($robot_score, $unused_minimum_scores);
            if (array_search($robot_score, $unused_minimum_scores) === false) {
                //  if the score isn't in the unused_scores array, then i remove the highest value in that array
                array_pop($unused_minimum_scores);
            } else {
                //  otherwise, i remove that score from the array
                array_splice($unused_minimum_scores, $robot_score_position, 1, null);
            }

            $used_scores[] = $robot_score;

            $this->log("value selected: [" . $min . ", " . $max . "] " . PHP_EOL);
            $this->log("value selected: " . $robot_score . PHP_EOL);
            $this->log("VALUE REMOVED FROM THE ARRAY (unused_scores): ". json_encode($unused_minimum_scores) . PHP_EOL);
            $this->log("VALUE ADDED TO THE ARRAY (used_scores): ". json_encode($used_scores) . PHP_EOL);
            $this->log("=====================================" . PHP_EOL);

            $robot = new Robot($robot_score);

            $roster[] = $robot;
            $roster_taken_scores[] = $robot_score;
        }

        $this->assignRolesToRoster($roster, $roster_roles);

        return $roster;
    }

    private function log($message, $restart_file = false) {
        if ($restart_file) {
            file_put_contents("roster_generation_log.txt", $message);
        } else {
            file_put_contents("roster_generation_log.txt", $message, 8);
        }
    }

    private function generateRosterRolesArray($starters, $substitutes) {
        $roster_roles = array();
        for ($i=0; $i<$starters; $i++) {
            $roster_roles[] = self::ROLE_STARTER;
        }
        for ($i=0; $i<$substitutes; $i++) {
            $roster_roles[] = self::ROLE_SUBSTITUTE;
        }
        return $roster_roles;
    }

    /**
     *  Orders the robot roster using their scores, in descending order
     */
    private function orderRosterByScore(array &$roster) {
        usort($roster, function(Robot $first_robot, Robot $second_robot) {
            return ($first_robot->getScore() < $second_robot->getScore()) ? 1 : -1;
        });
    }

    private function assignRolesToRoster(array &$roster, array $roster_roles) {
        //  I order the roster in score's descending order, since the best robots should be the starters
        $this->orderRosterByScore($roster);

        $roster_size = count($roster);
        for ($i=0; $i<$roster_size; $i++) {
            $role = array_shift($roster_roles);
            $roster[$i]->setRole($role);
        }
    }
}