<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class RosterForm extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('roster_max_points', TextType::class, array(
                'constraints' => array(
                    new Assert\NotBlank(),
                    new Assert\Range(array(
                        'min' => 0,
                        'minMessage' => 'This field must have a value higher or equal than {{ limit }}'
                    ))
                ),
            ))
            ->add('starters_quantity', TextType::class, array(
                'constraints' => array(
                    new Assert\NotBlank(),
                    new Assert\Range(array(
                        'min' => 1,
                        'minMessage' => 'This field must have a value higher or equal than {{ limit }}'
                    ))
                ),
            ))
            ->add('substitutes_quantity', TextType::class, array(
                'constraints' => array(
                    new Assert\NotBlank(),
                    new Assert\Range(array(
                        'min' => 1,
                        'minMessage' => 'This field must have a value higher or equal than {{ limit }}'
                    ))
                ),
            ))
            ->add('robot_max_score', TextType::class, array(
                'constraints' => array(
                    new Assert\NotBlank(),
                    new Assert\Range(array(
                        'min' => 0,
                        'minMessage' => 'This field must have a value higher or equal than {{ limit }}'
                    ))
                ),
            ))
            ;
    }

    public function configureOptions(OptionsResolver $resolver) {
    }
}